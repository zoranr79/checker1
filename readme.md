# MSISDN parser

## API

MSISDN parser, ki sem ga sprogramiral uporablja REST api. Zakaj sem vzel REST api? Ker se mi zdi najbolj pregledna verzija API-jev. V�e� mi je kako �e sama pot poka�e kaj je njegov cilj. Z GET, POST, PUT, DELETE pa hitro dobimo �e njegov namen.

## Usage

Z GET klicem na /api/msisdn/{msisdn_number}, program poi��e klicno �tevilko dr�ave, ime mobilnega operaterja in telefonsko �tevilko. 
�e je MSISDN �tevilka pravilna in v podtakih obstaja klicna �tevilka dr�ave in mobilnega operaterja, dobimo JSON odgovor v slede�i obliki (/api/msisdn/00386401234567):

```
{
	"error": false,
	"data": {
		"mno": "SI.Mobil",
		"country_code": "386",
		"subscriber_number": "1234567",
		"country": "si"
	}
}
```


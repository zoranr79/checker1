import java.awt.EventQueue;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.concurrent.TimeUnit;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import SocketServer.Client;
import SocketServer.ClientAdapter;
import Checker.Common;
import Checker.PinController;
import Gadgets.GadgetCommand;
import Gadgets.GadgetsController;


public class Checker1 { 

	private Client socketClient; 
	private PinController pinController;
	
	private GadgetsController gadgetsController;
	
	public Checker1() {

		initializeCommunication();
		initializeGadgets(); 
		
		do
		{
			try {
				TimeUnit.MILLISECONDS.sleep(900);
			} catch (InterruptedException e) {
				//e.printStackTrace();
			}	
			
			//Common.Echo("No of threads: " + Common.GetActiveThreads());
			//Common.Echo("Doing nothing....");
			if (socketClient != null) Common.Echo("Socket alive: " + socketClient.isAlive());
			
			
		}
		while (true);

	}
	
	
	private void initializeGadgets()
	{
		/* Init pin controller */
		pinController = new PinController();
		
		gadgetsController = new GadgetsController(pinController, socketClient);
		
	}
	
	
	
	private Thread comunicationThread;
	private String host = "192.168.2.100";
	private Integer port = 5002;
	private void initializeCommunication()
	{
		if (comunicationThread == null || !comunicationThread.isAlive())
		{
			comunicationThread = new Thread() {
			    public void run() {
			        	
			    	Common.Echo("startSocketServer");
					socketClient = new Client(host, port);
					socketClient.start();
					socketClient.addClientListener(new ClientAdapter() {
						@Override
						public void messageReceived(Client client, Object msg) {
							//Common.Echo("New message");
							
							if (msg instanceof GadgetCommand)
							{
								GadgetCommand gCommand = (GadgetCommand) msg;
								Gson gson = new GsonBuilder().setPrettyPrinting().create();
								String jsonOutput = gson.toJson(gCommand);
								//Common.Echo(jsonOutput);
								
								if (gCommand.Part.equals("Gadget"))
								{
									try {
										Class<?>[] paramTypes = new Class<?>[1];
										paramTypes[0] = gCommand.getClass();
										
										Field selectedFiled = gadgetsController.getClass().getField(gCommand.PartElement + "Controller");
										Method selectedMethod = selectedFiled.get(gadgetsController).getClass().getMethod(gCommand.RequestCommand, paramTypes);
										selectedMethod.invoke(selectedFiled.get(gadgetsController), gCommand);
										
									} catch (NoSuchFieldException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									} catch (SecurityException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									} catch (NoSuchMethodException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									} catch (IllegalAccessException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									} catch (IllegalArgumentException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									} catch (InvocationTargetException e) {
										// TODO Auto-generated catch block
										Common.Echo(e.getCause().toString());
										//e.printStackTrace();
									}
								}
							}
						}
					});
					socketClient.addClientListener(new ClientAdapter() {
						@Override
						public void disconnected(Client disconnectedClient) {
							Common.Echo("DISCONNECTED");
							return;
						}
					});
					
					CheckCommunication();
			    } 
			    
			    private void CheckCommunication()
			    {
			    	do
					{
						try {
							TimeUnit.MILLISECONDS.sleep(900);
						} 
						catch (InterruptedException e) {
							//e.printStackTrace();
						}	
						
				    	if (!socketClient.isAlive())
						{
				    		run();
						}
					}
					while (true);
			    }
			};
			comunicationThread.setDaemon(true);
			comunicationThread.start();
		}
		
	}

	
	public static Checker1 checker1;
	public boolean communicationInitialized = false;
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					checker1 = new Checker1();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
		
	}

}

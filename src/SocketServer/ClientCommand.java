package SocketServer;

public enum ClientCommand implements Command {

	HANDSHAKE, DISCONNECT
}

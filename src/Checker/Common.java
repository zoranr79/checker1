package Checker;

import java.util.List;

public class Common { 

	public static void Echo(String msg)
    {
        System.out.println(msg);
    }
	
	public static int GetActiveThreads()
	{
		int nbRunning = 0;
		for (Thread t : Thread.getAllStackTraces().keySet()) {
		    if (t.getState()==Thread.State.RUNNABLE) nbRunning++;
		}
		
		return nbRunning;
	}
	
	public static void ClearMinMax(List<Long> array) {
	    if (array == null || array.size() < 1)
	        return;
	    
	    Long max = array.get(0);
	    Long min = array.get(0);

	    Integer arrayLength = array.size();
	    for (int i = 1; i <= arrayLength - 1; i++) {
	        if (max < array.get(i)) {
	            max = array.get(i);
	        }

	        if (min > array.get(i)) {
	            min = array.get(i);
	        }
	    }
	   
   		array.remove(max);
   		array.remove(min);
	}
}

package Checker;

import java.util.HashMap;

import com.pi4j.io.gpio.GpioController;
import com.pi4j.io.gpio.GpioFactory;
import com.pi4j.io.gpio.GpioPinDigitalInput;
import com.pi4j.io.gpio.GpioPinDigitalOutput;
import com.pi4j.io.gpio.Pin;
import com.pi4j.io.gpio.PinState;
import com.pi4j.io.gpio.RaspiPin;

public class PinController { 

	public static  GpioController gpioController = null;
	
	
	
	
	public PinController()
	{
		gpioController = GpioFactory.getInstance();
	}
	
	
	public void DistanceMeasurementPins(HashMap<String, Object> pins, Pin triggerPin, Pin measurePin)
	{
		GpioPinDigitalOutput pinTrigger = (GpioPinDigitalOutput) pins.get("out"); 
		pinTrigger = gpioController.provisionDigitalOutputPin(triggerPin, "pinRangeSensorTrigger", PinState.LOW); 
		pins.put("out", (Object)pinTrigger);
		
		GpioPinDigitalInput pinMeasure = (GpioPinDigitalInput) pins.get("in"); 
		pinMeasure = gpioController.provisionDigitalInputPin(measurePin, "pinRangeSensorMeasure"); 
		pins.put("in", (Object)pinMeasure);
	}
	
	public void CollisionAvoidancePins(HashMap<String, Object> pins, Pin enablePin, Pin detectPin)
	{
		GpioPinDigitalOutput pinEnable = (GpioPinDigitalOutput) pins.get("out"); 
		pinEnable = gpioController.provisionDigitalOutputPin(enablePin, "pinEnable", PinState.HIGH); 
		pins.put("out", (Object)pinEnable);
		
		GpioPinDigitalInput pinDetect = (GpioPinDigitalInput) pins.get("in"); 
		pinDetect = gpioController.provisionDigitalInputPin(detectPin, "pinDetect"); //RaspiPin.GPIO_05
		pins.put("in", (Object)pinDetect);
	}
	
	public void DrivePins(HashMap<String, Object> pins, Pin leftForwardPin, Pin leftBackPin, Pin leftSpeedPin, Pin rightForwardPin, Pin rightBackPin, Pin rightSpeedPin)
	{
		GpioPinDigitalOutput pinLeftForward = (GpioPinDigitalOutput) pins.get("leftForward"); 
		pinLeftForward = gpioController.provisionDigitalOutputPin(leftForwardPin, "pinLeftForward", PinState.LOW); 
		pins.put("leftForward", (Object)pinLeftForward);
		
		GpioPinDigitalOutput pinLeftBack = (GpioPinDigitalOutput) pins.get("leftBack"); 
		pinLeftBack = gpioController.provisionDigitalOutputPin(leftBackPin, "pinLeftBack", PinState.LOW); 
		pins.put("leftBack", (Object)pinLeftBack);
		
		GpioPinDigitalOutput pinLeftSpeed = (GpioPinDigitalOutput) pins.get("leftSpeed"); 
		pinLeftSpeed = gpioController.provisionDigitalOutputPin(leftSpeedPin, "pinLeftSpeed", PinState.LOW); 
		pins.put("leftSpeed", (Object)pinLeftSpeed);
		
		GpioPinDigitalOutput pinRightForward = (GpioPinDigitalOutput) pins.get("rightForward"); 
		pinRightForward = gpioController.provisionDigitalOutputPin(rightForwardPin, "pinRightForward", PinState.LOW); 
		pins.put("rightForward", (Object)pinRightForward);
		
		
		GpioPinDigitalOutput pinRightBack = (GpioPinDigitalOutput) pins.get("rightBack"); 
		pinRightBack = gpioController.provisionDigitalOutputPin(rightBackPin, "pinRightBack", PinState.LOW); 
		pins.put("rightBack", (Object)pinRightBack);
		
		GpioPinDigitalOutput pinRightSpeed = (GpioPinDigitalOutput) pins.get("rightSpeed"); 
		pinRightSpeed = gpioController.provisionDigitalOutputPin(rightSpeedPin, "pinRightSpeed", PinState.LOW); 
		pins.put("rightSpeed", (Object)pinRightSpeed);
		
	}
}

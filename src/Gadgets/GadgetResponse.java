package Gadgets;

public class GadgetResponse implements java.io.Serializable { 

	public String Part;
	public String PartElement;
	public String RequestCommand;
	public String ResponseCommand;
	
	public Object ResponseData;
	
	public GadgetResponse()
	{		
		
	}
	
	public GadgetResponse(String part, String partElement, String requestCommand, String responseCommand, Object responseData)
	{
		this.Part = part;
		this.PartElement = partElement;
		this.RequestCommand = requestCommand;
		this.ResponseCommand = responseCommand;
		
		this.ResponseData = responseData;
	}
}

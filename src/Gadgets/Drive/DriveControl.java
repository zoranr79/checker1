package Gadgets.Drive;

import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

import Checker.Common;

import com.pi4j.io.gpio.GpioPinDigitalOutput;

public class DriveControl {

	private GpioPinDigitalOutput leftWheelsForward = null;
	private GpioPinDigitalOutput leftWheelsBack = null;
	private GpioPinDigitalOutput leftSpeed = null;
	private GpioPinDigitalOutput rightWheelsForward = null;
	private GpioPinDigitalOutput rightWheelsBack = null;
	private GpioPinDigitalOutput rightSpeed = null;
	
	public DriveControl(GpioPinDigitalOutput leftWheelsForward, GpioPinDigitalOutput leftWheelsBack, GpioPinDigitalOutput leftSpeed, 
			GpioPinDigitalOutput rightWheelsForward, GpioPinDigitalOutput rightWheelsBack, GpioPinDigitalOutput rightSpeed)
	{
		this.leftWheelsForward = leftWheelsForward;
		this.leftWheelsBack = leftWheelsBack;
		this.leftSpeed = leftSpeed;
		this.rightWheelsForward = rightWheelsForward;
		this.rightWheelsBack = rightWheelsBack;
		this.rightSpeed = rightSpeed;
	}
	
	
	private String direction = "";
	private Integer speed = 0;
	
	
	public Boolean Drive(String dir)
	{
		return Drive(dir, 64);
	}
	public Boolean Drive(String dir, Integer speed)
	{
		this.direction = dir;
		this.speed = speed;
		
		Boolean driving = false;
		switch(dir)
		{
			case "forward":
			{
				Stop();
				
				this.leftWheelsForward.high();
				this.rightWheelsForward.high();	
				driving = true;
				
				break;
			}
			
			case "back":
			{
				Stop();
				
				this.leftWheelsBack.high();
				this.rightWheelsBack.high();
				driving = true;
				break;
			}
			
			case "left":
			{
				Stop();
				
				this.rightWheelsForward.high();
				this.leftWheelsBack.high();
				driving = true;
				break;
			}
			
			case "right":
			{
				Stop();
				
				this.leftWheelsForward.high();
				this.rightWheelsBack.high();
				driving = true;
				break;
			}
			
			case "stop":
			{
				Stop();		
				break;
			}
		}
		
		if (driving)
		{
			PWDDrive();
		}
		
		return driving;
	}
	
	
	public Long periode = (long) 60.0;
	java.util.Timer pwdDriverTimer;
	
	private void PWDDrive()
	{
		Long pulseWidth = GetPulseSpeed(this.speed);
		
		if (pwdDriverTimer != null)
		{
			pwdDriverTimer.cancel();
			pwdDriverTimer.purge();
		}
		pwdDriverTimer = new java.util.Timer();
		pwdDriverTimer.schedule(new PwdDriveTimerTask(this.direction, this.leftSpeed, this.rightSpeed, pulseWidth) {

            @Override
            public void run() {
            	//this.getLeftSpeedPin().pulse(this.getPulseWidth());
            	//this.getRightSpeedPin().pulse(this.getPulseWidth());
            	this.getLeftSpeedPin().high();
            	this.getRightSpeedPin().high();
            	try {
        			TimeUnit.MILLISECONDS.sleep(this.getPulseWidth());
        		} catch (InterruptedException e) {
        			e.printStackTrace();
        		}	
            	this.getLeftSpeedPin().low();
            	this.getRightSpeedPin().low();
            }
        }, 0, this.periode);
	}
	
	class PwdDriveTimerTask extends TimerTask  {
		private String direction;
	    private GpioPinDigitalOutput leftSpeed;
	    private GpioPinDigitalOutput rightSpeed;
	    
	    private Long pulseWidth = (long) 0;
	    
	     public PwdDriveTimerTask(String direction, GpioPinDigitalOutput leftSpeed, GpioPinDigitalOutput rightSpeed, Long pulseWidth) {
	    	 this.setDirection(direction);
	    	 this.setLeftSpeedPin(leftSpeed);
	    	 this.setRightSpeedPin(rightSpeed);
	    	 this.setPulseWidth(pulseWidth);
	     }

	     @Override
	     public void run() {
	    	 
	     }

		public String getDirection() {
			return this.direction;
		}

		public void setDirection(String direction) {
			this.direction = direction;
		}

		public GpioPinDigitalOutput getLeftSpeedPin() {
			return this.leftSpeed;
		}

		public void setLeftSpeedPin(GpioPinDigitalOutput leftSpeed) {
			this.leftSpeed = leftSpeed;
		}
		
		public GpioPinDigitalOutput getRightSpeedPin() {
			return this.rightSpeed;
		}

		public void setRightSpeedPin(GpioPinDigitalOutput rightSpeed) {
			this.rightSpeed = rightSpeed;
		}
		
		public Long getPulseWidth() {
			return this.pulseWidth;
		}

		public void setPulseWidth(Long pulseWidth) {
			this.pulseWidth = pulseWidth;
		}
	}
	
	private Long GetPulseSpeed(Integer speed)
	{
		Long pulseWidth = (long) (((double) this.periode / (double) 255) * (double) speed);
		return pulseWidth;
	}
	
	private void Stop()
	{
		if (pwdDriverTimer != null)
		{
			pwdDriverTimer.cancel();
			pwdDriverTimer.purge();
		}
		this.leftWheelsForward.low();
		this.rightWheelsForward.low();
		this.leftWheelsBack.low();
		this.rightWheelsBack.low();
	}
}

package Gadgets.Drive;

import java.util.HashMap;
import java.util.Hashtable;

import Checker.Common;
import Gadgets.GadgetCommand;
import Gadgets.GadgetCommunication;
import Gadgets.GadgetResponse;
import Gadgets.CollisionAvoidance.CollisionAvoidanceControl;

import com.google.gson.Gson;
import com.pi4j.io.gpio.GpioPinDigitalInput;
import com.pi4j.io.gpio.GpioPinDigitalOutput;

public class DriveController {
	public String UniqueControllerId = "";
	
	private DriveControl driveControl;
	
	private GpioPinDigitalOutput leftWheelsForward = null;
	private GpioPinDigitalOutput leftWheelsBack = null;
	private GpioPinDigitalOutput leftSpeed = null;
	private GpioPinDigitalOutput rightWheelsForward = null;
	private GpioPinDigitalOutput rightWheelsBack = null;
	private GpioPinDigitalOutput rightSpeed = null;
	
	private GadgetCommunication gadgetCommunication;
	
	public DriveController(String uniqueControllerId, HashMap<String, Object> pins, GadgetCommunication gadgetCommunication)
	{
		this.UniqueControllerId = uniqueControllerId;
		this.gadgetCommunication = gadgetCommunication;
		
		initPins(pins);

		this.driveControl = new DriveControl(leftWheelsForward, leftWheelsBack, leftSpeed, rightWheelsForward, rightWheelsBack, rightSpeed);
	}
	
	private void initPins(HashMap<String, Object> pins)
	{
		this.leftWheelsForward = (GpioPinDigitalOutput) pins.get("leftForward");
		this.leftWheelsBack = (GpioPinDigitalOutput) pins.get("leftBack");
		this.leftSpeed = (GpioPinDigitalOutput) pins.get("leftSpeed");
		
		this.rightWheelsForward = (GpioPinDigitalOutput) pins.get("rightForward");
		this.rightWheelsBack = (GpioPinDigitalOutput) pins.get("rightBack");
		this.rightSpeed = (GpioPinDigitalOutput) pins.get("rightSpeed");
	}
	
	public void Drive(GadgetCommand gCommand)
	{
		Gson gson = new Gson();
		
		Hashtable<String, String> requestData = gson.fromJson((String) gCommand.RequestData, Hashtable.class);
		String direction = (String) requestData.get("direction");
		if (direction == null || direction == "") direction = "stop";
		
		Integer speed = Integer.parseInt(requestData.get("speed"));
		if (speed <= 0) speed = 64;
		else if (speed > 128) speed = 128;
		
		Common.Echo("drive: " + direction + "; speed: " + speed.toString());
		
		Boolean driving = this.driveControl.Drive(direction, speed);
		
		Hashtable<String, String> responseData = new Hashtable<>();
		responseData.put("direction", direction);
		responseData.put("driving", Boolean.toString(driving));
		
		GadgetResponse gadgetResponse = new GadgetResponse();
		gadgetResponse.Part = "Gadget";
		gadgetResponse.PartElement = this.UniqueControllerId;
		gadgetResponse.RequestCommand = "SingleMeasurement";
		gadgetResponse.ResponseCommand = gCommand.ResponseCommand;
		gadgetResponse.ResponseData = gson.toJson(responseData);
		
		this.gadgetCommunication.SendData(gadgetResponse);
		
	}
}

package Gadgets;

import java.util.HashMap;

import com.pi4j.io.gpio.GpioPinDigitalInput;
import com.pi4j.io.gpio.GpioPinDigitalOutput;
import com.pi4j.io.gpio.RaspiPin;

import Checker.PinController;
import Gadgets.DistanceMeasurement.DistanceMeasurementController;
import Gadgets.CollisionAvoidance.CollisionAvoidanceController;
import Gadgets.Drive.DriveController;
import Gadgets.RemoteControl.RemoteControlController;
import Gadgets.TempAndHum.TempAndHumController;
import SocketServer.Client;

public class GadgetsController {

	private PinController pinController;
	
	private Client socketClient;
	public GadgetCommunication gadgetCommunication;
	
	public GadgetsController(PinController pinController, Client socketClient)
	{
		this.pinController = pinController;
		
		this.socketClient = socketClient;
		this.gadgetCommunication = new GadgetCommunication(this.socketClient);
		
		initControllers();
	}
	
	

	public DistanceMeasurementController DistanceMeasurementFrontController;
	private GpioPinDigitalOutput pinRangeSensorTrigger_Front = null;
	private GpioPinDigitalInput pinRangeSensorMeasure_Front = null;
	
	public DistanceMeasurementController DistanceMeasurementLeftController;
	private GpioPinDigitalOutput pinRangeSensorTrigger_Left = null;
	private GpioPinDigitalInput pinRangeSensorMeasure_Left = null;

	public CollisionAvoidanceController CollisionAvoidanceController;
	private GpioPinDigitalOutput pinCollisionDetectEnable = null;
	private GpioPinDigitalInput pinCollisionDetect = null;
	
	public DriveController DriveController;
	private GpioPinDigitalOutput leftForward = null;
	private GpioPinDigitalOutput leftBack = null;
	private GpioPinDigitalOutput leftSpeed = null;
	private GpioPinDigitalOutput rightForward = null;
	private GpioPinDigitalOutput rightBack = null;
	private GpioPinDigitalOutput rightSpeed = null;
	
	public TempAndHumController TempAndHumController;
	
	public RemoteControlController RemoteControlDnevnaTvController;
	public RemoteControlController RemoteControlDnevnaTvOldController;
	
	private void initControllers()
	{	
		HashMap<String, Object> pinsFront = new HashMap<>();
		pinsFront.put("out", pinRangeSensorTrigger_Front);
		pinsFront.put("in", pinRangeSensorMeasure_Front);
		this.pinController.DistanceMeasurementPins(pinsFront, RaspiPin.GPIO_06, RaspiPin.GPIO_10);
		this.DistanceMeasurementFrontController = new DistanceMeasurementController("DistanceMeasurementFront", pinsFront, this.gadgetCommunication);
		
		HashMap<String, Object> pinsLeft = new HashMap<>();
		pinsLeft.put("out", pinRangeSensorTrigger_Left);
		pinsLeft.put("in", pinRangeSensorMeasure_Left);
		this.pinController.DistanceMeasurementPins(pinsLeft, RaspiPin.GPIO_00, RaspiPin.GPIO_02);
		this.DistanceMeasurementLeftController = new DistanceMeasurementController("DistanceMeasurementLeft" ,pinsLeft, this.gadgetCommunication);
	
		HashMap<String, Object> pinsCollision = new HashMap<>();
		pinsCollision.put("out", pinCollisionDetectEnable);
		pinsCollision.put("in", pinCollisionDetect);
		this.pinController.CollisionAvoidancePins(pinsCollision, RaspiPin.GPIO_11, RaspiPin.GPIO_31);
		this.CollisionAvoidanceController = new CollisionAvoidanceController("CollisionAvoidance" ,pinsCollision, this.gadgetCommunication);
		
		HashMap<String, Object> pinsDrive = new HashMap<>();
		pinsDrive.put("leftForward", leftForward);
		pinsDrive.put("leftBack", leftBack);
		pinsDrive.put("leftSpeed", leftSpeed);
		pinsDrive.put("rightForward", rightForward);
		pinsDrive.put("rightBack", rightBack);
		pinsDrive.put("rightSpeed", rightSpeed);
		this.pinController.DrivePins(pinsDrive, RaspiPin.GPIO_21, RaspiPin.GPIO_24, RaspiPin.GPIO_25, RaspiPin.GPIO_27, RaspiPin.GPIO_28, RaspiPin.GPIO_29);
		this.DriveController = new DriveController("Drive" ,pinsDrive, this.gadgetCommunication);
		
		HashMap<String, Object> pinsTempAndHum = new HashMap<>(); // GPIO_01
		this.TempAndHumController = new TempAndHumController("TempAndHum" ,pinsTempAndHum, this.gadgetCommunication);
		
		HashMap<String, Object> pinsRemoteControl = new HashMap<>(); // GPIO_04, GPIO_05
		this.RemoteControlDnevnaTvController = new RemoteControlController("RemoteControlDnevnaTv" , pinsRemoteControl, this.gadgetCommunication);
		this.RemoteControlDnevnaTvOldController = new RemoteControlController("RemoteControlDnevnaTvOld" , pinsRemoteControl, this.gadgetCommunication);
	}
}

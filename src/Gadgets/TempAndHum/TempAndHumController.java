package Gadgets.TempAndHum;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.TimerTask;

import Checker.Common;
import Gadgets.GadgetCommand;
import Gadgets.GadgetCommunication;
import Gadgets.GadgetResponse;

import com.google.gson.Gson;

public class TempAndHumController {
	
	public String UniqueControllerId = "";
	
	private GadgetCommunication gadgetCommunication;
	
	public TempAndHumController(String uniqueControllerId, HashMap<String, Object> pins, GadgetCommunication gadgetCommunication)
	{
		this.UniqueControllerId = uniqueControllerId;
		this.gadgetCommunication = gadgetCommunication;
		
		initPins(pins);

	}
	
	private void initPins(HashMap<String, Object> pins)
	{
		
	}
	
	public void SingleMeasurement(GadgetCommand gCommand)
	{
		Gson gson = new Gson();
		
		Hashtable<String, String> requestData = gson.fromJson((String) gCommand.RequestData, Hashtable.class);
		Integer average = Integer.parseInt(requestData.get("average"));
		
		HashMap<String, Double> tempAndHumData = ReadTempAndHum();
		
		Common.Echo("temp: " + tempAndHumData.get("temp") + "; hum: " + tempAndHumData.get("hum"));
		
		Hashtable<String, String> responseData = new Hashtable<>();
		responseData.put("tempAndHum", gson.toJson(tempAndHumData));
		
		GadgetResponse gadgetResponse = new GadgetResponse();
		gadgetResponse.Part = "Gadget";
		gadgetResponse.PartElement = this.UniqueControllerId;
		gadgetResponse.RequestCommand = "SingleMeasurement";
		gadgetResponse.ResponseCommand = gCommand.ResponseCommand;
		gadgetResponse.ResponseData = gson.toJson(responseData);
		
		this.gadgetCommunication.SendData(gadgetResponse);
	}
	
	public Integer periode = 30000;
	java.util.Timer continuesMeasurementTimer;
	
	public void ContinuesMeasurement(GadgetCommand gCommand) {
		Gson gson = new Gson();
		
		Hashtable<String, String> requestData = gson.fromJson((String) gCommand.RequestData, Hashtable.class);
		
		Integer average = Integer.parseInt(requestData.get("average"));
		Integer periodeTmp = Integer.parseInt(requestData.get("periode"));
		if (periodeTmp > 0) periode = periodeTmp;
		Common.Echo("periode: " + periode);
		
		if (continuesMeasurementTimer != null) 
		{
			continuesMeasurementTimer.cancel();
			continuesMeasurementTimer.purge();
		}
		continuesMeasurementTimer = new java.util.Timer();
		continuesMeasurementTimer.schedule(new ContinuesMeasurementTimerTask(average, gCommand, this.gadgetCommunication) {

            @Override
            public void run() {
	   	    	Gson gson = new Gson();
	   			
	   	    	Integer average = this.getAverage();
	   	    	if (average < 3) average = 3; 
	   			
	   	    	HashMap<String, Double> tempAndHumData = ReadTempAndHum();
	   	    	Common.Echo("temp: " + tempAndHumData.get("temp") + "; hum: " + tempAndHumData.get("hum"));
	   			
	   			Hashtable<String, String> responseData = new Hashtable<>();
	   			responseData.put("tempAndHum", gson.toJson(tempAndHumData));
	   			
	   			GadgetResponse gadgetResponse = new GadgetResponse();
	   			gadgetResponse.Part = "Gadget";
	   			gadgetResponse.PartElement = UniqueControllerId;
	   			gadgetResponse.RequestCommand = "ContinuesMeasurement";
	   			gadgetResponse.ResponseCommand = this.getgCommand().ResponseCommand;
	   			gadgetResponse.ResponseData = gson.toJson(responseData);
	
	   			this.getGadgetCommunication().SendData(gadgetResponse);
            }
        }, 0, periode);
	}
	
	class ContinuesMeasurementTimerTask extends TimerTask  {
		private int average;
	    private GadgetCommand gCommand;
	    private GadgetCommunication gadgetCommunication;
	    
	     public ContinuesMeasurementTimerTask(int average, GadgetCommand gCommand, GadgetCommunication gadgetCommunication) {
	    	 this.setAverage(average);
		     this.setgCommand(gCommand);
		     this.setGadgetCommunication(gadgetCommunication);
	     }

	     @Override
	     public void run() {
	    	 
	     }

	     public int getAverage() {
	    	 return average;
	     }

	     public void setAverage(int average) {
	    	 this.average = average;
	     }

	     public GadgetCommunication getGadgetCommunication() {
	    	 return gadgetCommunication;
	     }

	     public void setGadgetCommunication(GadgetCommunication gadgetCommunication) {
	    	 this.gadgetCommunication = gadgetCommunication;
	     }

	     public GadgetCommand getgCommand() {
	    	 return gCommand;
	     }
		
	     public void setgCommand(GadgetCommand gCommand) {
	    	 this.gCommand = gCommand;
	     }
	}
	
	public void StopContinuesMeasurement(GadgetCommand gCommand) {
		Gson gson = new Gson();

		Hashtable<String, String> responseData = new Hashtable<>();
		
		if (continuesMeasurementTimer != null) 
		{
			continuesMeasurementTimer.cancel();
			continuesMeasurementTimer.purge();
		}
		
		GadgetResponse gadgetResponse = new GadgetResponse();
		gadgetResponse.Part = "Gadget";
		gadgetResponse.PartElement = UniqueControllerId;
		gadgetResponse.RequestCommand = "StopContinuesMeasurement";
		gadgetResponse.ResponseCommand = gCommand.ResponseCommand;
		gadgetResponse.ResponseData = gson.toJson(responseData);

		this.gadgetCommunication.SendData(gadgetResponse);
	}
	
	
	private HashMap<String, Double> ReadTempAndHum()
	{
		HashMap<String, Double> data = null;
		
		Process p;
		try {
			p = Runtime.getRuntime().exec("sudo ./dht");
		    p.waitFor();
		    BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
		    String line = "";
		    while ((line = reader.readLine())!= null) { 
		    	if (line != "" && line.contains(";")) data = ParseLine(line);
		    }
		} 
		catch (Exception e) {
			e.printStackTrace();
		}
		
		return data;
	}
	
	private HashMap<String, Double> ParseLine(String line)
	{
		HashMap<String, Double> data = new HashMap<>();
		data.put("temp", (double) 0);
		data.put("hum", (double) 0);
		
		String[] dataSplit = line.split(";");
		if (dataSplit.length == 2)
		{
			String hum = dataSplit[0].replace("H=", "");
			data.put("hum", Double.parseDouble(hum));
			
			String temp = dataSplit[1].replace("T=", "");
			data.put("temp", Double.parseDouble(temp));
		}
		return data;
	}
}

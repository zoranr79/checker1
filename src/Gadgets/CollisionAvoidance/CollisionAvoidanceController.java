package Gadgets.CollisionAvoidance;

import java.util.HashMap;
import java.util.Hashtable;

import Checker.Common;
import Gadgets.GadgetCommand;
import Gadgets.GadgetCommunication;
import Gadgets.GadgetResponse;

import com.google.gson.Gson;
import com.pi4j.io.gpio.GpioPinDigitalInput;
import com.pi4j.io.gpio.GpioPinDigitalOutput;

public class CollisionAvoidanceController {
	
	public String UniqueControllerId = "";
	
	private CollisionAvoidanceControl collisionAvoidanceControl;
	
	private GpioPinDigitalOutput pinCollisionDetectEnable = null;
	private GpioPinDigitalInput pinCollisionDetect = null;
	
	private GadgetCommunication gadgetCommunication;
	
	public CollisionAvoidanceController(String uniqueControllerId, HashMap<String, Object> pins, GadgetCommunication gadgetCommunication)
	{
		this.UniqueControllerId = uniqueControllerId;
		this.gadgetCommunication = gadgetCommunication;
		
		initPins(pins);
		
		collisionAvoidanceControl = new CollisionAvoidanceControl(pinCollisionDetectEnable, pinCollisionDetect);
	}
	
	private void initPins(HashMap<String, Object> pins)
	{
		this.pinCollisionDetectEnable = (GpioPinDigitalOutput) pins.get("out");
		this.pinCollisionDetect = (GpioPinDigitalInput) pins.get("in");
	}
	
	public void SingleMeasurement(GadgetCommand gCommand)
	{
		Gson gson = new Gson();
		
		Hashtable<String, String> requestData = gson.fromJson((String) gCommand.RequestData, Hashtable.class);
		
		collisionAvoidanceControl.StartMeasurement();
		Boolean collision = collisionAvoidanceControl.GetData();
		
		Common.Echo("collision: " + collision);
		
		Hashtable<String, String> responseData = new Hashtable<>();
		responseData.put("collision", Boolean.toString(collision));
		
		GadgetResponse gadgetResponse = new GadgetResponse();
		gadgetResponse.Part = "Gadget";
		gadgetResponse.PartElement = this.UniqueControllerId;
		gadgetResponse.RequestCommand = "SingleMeasurement";
		gadgetResponse.ResponseCommand = gCommand.ResponseCommand;
		gadgetResponse.ResponseData = gson.toJson(responseData);
		
		this.gadgetCommunication.SendData(gadgetResponse);
	}
	
}

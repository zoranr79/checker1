package Gadgets.CollisionAvoidance;

import java.util.concurrent.TimeUnit;

import com.pi4j.io.gpio.GpioPinDigitalInput;
import com.pi4j.io.gpio.GpioPinDigitalOutput;
import com.pi4j.io.gpio.PinState;

public class CollisionAvoidanceControl { 
	
	private GpioPinDigitalOutput pinCollisionDetectEnable = null;
	private GpioPinDigitalInput pinCollisionDetect = null;

	private Boolean detectorActive = false;
	
	public CollisionAvoidanceControl(GpioPinDigitalOutput pinEnable, GpioPinDigitalInput pinDetect) {
		
		this.pinCollisionDetectEnable = pinEnable;
		this.pinCollisionDetect = pinDetect;
		
		
	}
	
	public void StartMeasurement() {

		this.check();

	}
	
	public Boolean GetData() {
		return this.collision;
	}
	
	
	private Boolean collision = true;
	
	private void check() {
		this.pinCollisionDetectEnable.high();
		
		try {
			TimeUnit.MICROSECONDS.sleep(500000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}	
		
		if (pinCollisionDetect.getState() == PinState.LOW) this.collision = true;
		else this.collision = false;
		
		this.pinCollisionDetectEnable.low();
	}
}

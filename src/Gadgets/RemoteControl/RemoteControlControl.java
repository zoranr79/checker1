package Gadgets.RemoteControl;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Hashtable;

import Checker.Common;

public class RemoteControlControl {

	private Hashtable<String, String> commandData;
	
	private String remoteControlId;
	private String command;
	
	
	public RemoteControlControl()
	{
		
	}
	
	public void SetCommandData(Hashtable<String, String> commandData)
	{
		this.commandData = commandData;
	}

	public void ParseCommandData() 
	{
		this.remoteControlId = (String) this.commandData.get("remoteControlId");
		this.command = (String) this.commandData.get("command");
		Common.Echo("remoteControlId: " + remoteControlId + "; command: " + command);
	}
	
	public Boolean ExecuteCommandData() 
	{
		HashMap<String, String> data = this.executeRemoteCommand();
		
		if (data.size() == 0) return true;
		return false;
	}
	
	private HashMap<String, String> executeRemoteCommand()
	{
		HashMap<String, String> data = new HashMap<>();
		
		String remoteCommand = "irsend SEND_ONCE " + this.remoteControlId + " " + this.command;
		
		Process p;
		try {
			p = Runtime.getRuntime().exec(remoteCommand);
		    p.waitFor();
		    BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
		   
		    String line = "";
		    int lineCount = 0;
		    
		    while ((line = reader.readLine())!= null) { 
		    	if (line != "") 
		    	{ 
		    		data = ParseLine(line, data, lineCount);
		    		lineCount++;
		    	}
		    }
		} 
		catch (Exception e) {
			e.printStackTrace();
		}
		
		return data;
	}
	
	private HashMap<String, String> ParseLine(String line, HashMap<String, String> data, int lineNum)
	{
		data.put("line-" + lineNum, line);
		return data;
	}
}

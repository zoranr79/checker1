package Gadgets.RemoteControl;

import java.util.HashMap;
import java.util.Hashtable;

import Checker.Common;
import Gadgets.GadgetCommand;
import Gadgets.GadgetCommunication;
import Gadgets.GadgetResponse;
import Gadgets.CollisionAvoidance.CollisionAvoidanceControl;

import com.google.gson.Gson;
import com.pi4j.io.gpio.GpioPinDigitalInput;
import com.pi4j.io.gpio.GpioPinDigitalOutput;

public class RemoteControlController {
	
	public String UniqueControllerId = "";
	
	private RemoteControlControl remoteControlControl;
	
	private GadgetCommunication gadgetCommunication;
	
	public RemoteControlController(String uniqueControllerId, HashMap<String, Object> pins, GadgetCommunication gadgetCommunication)
	{
		this.UniqueControllerId = uniqueControllerId;
		this.gadgetCommunication = gadgetCommunication;
		
		
		this.remoteControlControl = new RemoteControlControl();
	}
	
	public void ButtonClick(GadgetCommand gCommand)
	{
		Gson gson = new Gson();
		
		Hashtable<String, String> requestData = gson.fromJson((String) gCommand.RequestData, Hashtable.class);
		
		this.remoteControlControl.SetCommandData(requestData);
		this.remoteControlControl.ParseCommandData();
		Boolean executed = this.remoteControlControl.ExecuteCommandData();
		
		
		Hashtable<String, String> responseData = new Hashtable<>();
		responseData.put("executed", Boolean.toString(executed));
		
		GadgetResponse gadgetResponse = new GadgetResponse();
		gadgetResponse.Part = "Gadget";
		gadgetResponse.PartElement = this.UniqueControllerId;
		gadgetResponse.RequestCommand = "SingleMeasurement";
		gadgetResponse.ResponseCommand = gCommand.ResponseCommand;
		gadgetResponse.ResponseData = gson.toJson(responseData);
		
		this.gadgetCommunication.SendData(gadgetResponse);
	}
}

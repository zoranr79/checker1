package Gadgets;

import java.io.Serializable;
import java.net.Socket;

import SocketServer.Client;


public class GadgetCommunication {

	public Client socketClient;
	
	public GadgetCommunication(Client socketClient)
	{
		this.socketClient = socketClient;
	}
	
	public Boolean SendString(String response)
	{
		if (socketClient.send(response))
		{
			return true;	
		}
		return false;	
	}
	public Boolean SendData(GadgetResponse gadgetResponse)
	{
		if (socketClient.send(gadgetResponse))
		{
			return true;	
		}
		return false;	
	}
}

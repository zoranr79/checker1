package Gadgets.DistanceMeasurement;

import java.util.concurrent.TimeUnit;
import org.apache.commons.lang3.time.StopWatch;

import Checker.Common;

import com.pi4j.io.gpio.GpioPinDigitalInput;
import com.pi4j.io.gpio.GpioPinDigitalOutput;
import com.pi4j.io.gpio.PinState;

public class DistanceMeasurementControl {
	
	private GpioPinDigitalOutput pinRangeSensorTrigger = null;
	private GpioPinDigitalInput pinRangeSensorMeasure = null;

	private Boolean measureActive = false;
	
	public DistanceMeasurementControl(GpioPinDigitalOutput pinTrigger, GpioPinDigitalInput pinMeasure) {
		
		this.pinRangeSensorTrigger = pinTrigger;
		this.pinRangeSensorMeasure = pinMeasure;
		
		
	}
	
	public void StartMeasurement() {

		this.measure();

	}
	
	public long GetDistance() {
		return this.distance;
	}
	
	
	private long distance;
	
	private void measure() {
		StopWatch timer = new StopWatch();
		
		pinRangeSensorTrigger.high();
		try {
			TimeUnit.MICROSECONDS.sleep(10);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}		
		pinRangeSensorTrigger.low();
		while (pinRangeSensorMeasure.getState() == PinState.LOW) {}
		timer.start();
				
		while (pinRangeSensorMeasure.getState() == PinState.HIGH) {}
		timer.stop();
		Long pulseDuration = timer.getNanoTime() / 1000;
		distance = pulseDuration * 17150 / 1000000;
		distance = Math.round(distance);
	}
	
}

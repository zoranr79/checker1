package Gadgets.DistanceMeasurement;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

import Checker.Common;
import Checker.PinController;
import Gadgets.GadgetCommand;
import Gadgets.GadgetCommunication;
import Gadgets.GadgetResponse;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.pi4j.io.gpio.GpioPinDigitalInput;
import com.pi4j.io.gpio.GpioPinDigitalOutput;

public class DistanceMeasurementController {

	public String UniqueControllerId = "";
	
	private DistanceMeasurementControl distanceMeasurementControl;
	
	private GpioPinDigitalOutput pinRangeSensorTrigger = null;
	private GpioPinDigitalInput pinRangeSensorMeasure = null;
	
	private GadgetCommunication gadgetCommunication;
	
	public DistanceMeasurementController(String uniqueControllerId, HashMap<String, Object> pins, GadgetCommunication gadgetCommunication)
	{
		this.UniqueControllerId = uniqueControllerId;
		this.gadgetCommunication = gadgetCommunication;
		
		initPins(pins);
		
		distanceMeasurementControl = new DistanceMeasurementControl(pinRangeSensorTrigger, pinRangeSensorMeasure);
	}
	
	private void initPins(HashMap<String, Object> pins)
	{
		this.pinRangeSensorTrigger = (GpioPinDigitalOutput) pins.get("out");
		this.pinRangeSensorMeasure = (GpioPinDigitalInput) pins.get("in");
	}
	
	
	public void SingleMeasurement(GadgetCommand gCommand)
	{
		Gson gson = new Gson();
		
		Hashtable<String, String> requestData = gson.fromJson((String) gCommand.RequestData, Hashtable.class);
		
		Integer average = Integer.parseInt(requestData.get("average"));
		if (average < 3) average = 3; 
		
		Long distanceAverage = (long) 0;
		ArrayList<Long> distancesList = new ArrayList<>();
		for(Integer i = 0; i < average; i++) {
			try {
				TimeUnit.MILLISECONDS.sleep(40);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}	
			distanceMeasurementControl.StartMeasurement();
			distancesList.add(distanceMeasurementControl.GetDistance());
		}
		Common.ClearMinMax(distancesList);
		for(Integer i = 0; i < distancesList.size(); i++)
		{
			distanceAverage += distancesList.get(i);
		}
		distanceAverage = distanceAverage / distancesList.size();
		
		//Common.Echo("distance: " + distanceAverage);
		
		Hashtable<String, String> responseData = new Hashtable<>();
		responseData.put("distance", Long.toString(distanceAverage));
		
		GadgetResponse gadgetResponse = new GadgetResponse();
		gadgetResponse.Part = "Gadget";
		gadgetResponse.PartElement = this.UniqueControllerId;
		gadgetResponse.RequestCommand = "SingleMeasurement";
		gadgetResponse.ResponseCommand = gCommand.ResponseCommand;
		gadgetResponse.ResponseData = gson.toJson(responseData);
		
		this.gadgetCommunication.SendData(gadgetResponse);
	}
	
	
	//private javax.swing.Timer MeasureInterval;
	//private Boolean measureActive = false;
	public int periode = 200;
	java.util.Timer continuesMeasurementTimer;
	
	public void ContinuesMeasurement(GadgetCommand gCommand) {
		Gson gson = new Gson();
		
		Hashtable<String, String> requestData = gson.fromJson((String) gCommand.RequestData, Hashtable.class);
		
		Integer average = Integer.parseInt(requestData.get("average"));
		periode = Integer.parseInt(requestData.get("periode"));
		
		if (continuesMeasurementTimer != null) continuesMeasurementTimer.cancel();
		continuesMeasurementTimer = new java.util.Timer();
		continuesMeasurementTimer.schedule(new ContinuesMeasurementTimerTask(average, gCommand, this.gadgetCommunication) {

            @Override
            public void run() {
	   	    	Gson gson = new Gson();
	   			
	   	    	Integer average = this.getAverage();
	   	    	if (average < 3) average = 3; 
	   			
	   			Long distanceAverage = (long) 0;
	   			ArrayList<Long> distancesList = new ArrayList<>();
	   			for(Integer i = 0; i < average; i++) {
	   				try {
	   					TimeUnit.MILLISECONDS.sleep(40);
	   				} catch (InterruptedException e) {
	   					e.printStackTrace();
	   				}	
	   				distanceMeasurementControl.StartMeasurement();
	   				distancesList.add(distanceMeasurementControl.GetDistance());
	   			}
	   			Common.ClearMinMax(distancesList);
	   			for(Integer i = 0; i < distancesList.size(); i++)
	   			{
	   				distanceAverage += distancesList.get(i);
	   			}
	   			distanceAverage = distanceAverage / distancesList.size();
	   			
	   			//Common.Echo("distance: " + distanceAverage);
	
	   			Hashtable<String, String> responseData = new Hashtable<>();
	   			responseData.put("distance", Long.toString(distanceAverage));
	   			
	   			GadgetResponse gadgetResponse = new GadgetResponse();
	   			gadgetResponse.Part = "Gadget";
	   			gadgetResponse.PartElement = UniqueControllerId;
	   			gadgetResponse.RequestCommand = "ContinuesMeasurement";
	   			gadgetResponse.ResponseCommand = this.getgCommand().ResponseCommand;
	   			gadgetResponse.ResponseData = gson.toJson(responseData);
	
	   			this.getGadgetCommunication().SendData(gadgetResponse);
            }
        }, 0, periode);
	}
	
	class ContinuesMeasurementTimerTask extends TimerTask  {
		private int average;
	    private GadgetCommand gCommand;
	    private GadgetCommunication gadgetCommunication;
	    
	     public ContinuesMeasurementTimerTask(int average, GadgetCommand gCommand, GadgetCommunication gadgetCommunication) {
	    	 this.setAverage(average);
		     this.setgCommand(gCommand);
		     this.setGadgetCommunication(gadgetCommunication);
	     }

	     @Override
	     public void run() {
	    	 
	     }

		public int getAverage() {
			return average;
		}

		public void setAverage(int average) {
			this.average = average;
		}

		public GadgetCommunication getGadgetCommunication() {
			return gadgetCommunication;
		}

		public void setGadgetCommunication(GadgetCommunication gadgetCommunication) {
			this.gadgetCommunication = gadgetCommunication;
		}

		public GadgetCommand getgCommand() {
			return gCommand;
		}

		public void setgCommand(GadgetCommand gCommand) {
			this.gCommand = gCommand;
		}
	}
	
	public void StopContinuesMeasurement(GadgetCommand gCommand) {
		Gson gson = new Gson();

		Hashtable<String, String> responseData = new Hashtable<>();
		
		if (continuesMeasurementTimer != null) continuesMeasurementTimer.cancel();
		
		GadgetResponse gadgetResponse = new GadgetResponse();
		gadgetResponse.Part = "Gadget";
		gadgetResponse.PartElement = UniqueControllerId;
		gadgetResponse.RequestCommand = "StopContinuesMeasurement";
		gadgetResponse.ResponseCommand = gCommand.ResponseCommand;
		gadgetResponse.ResponseData = gson.toJson(responseData);

		this.gadgetCommunication.SendData(gadgetResponse);
	}
	
}



package Gadgets;

public class GadgetCommand implements java.io.Serializable { 

	public String Part;
	public String PartElement;
	public String RequestCommand;
	public String ResponseCommand;
	
	public Object RequestData; 
	
	public GadgetCommand()
	{
		
	}
	
	public GadgetCommand(String part, String partElement, String requestCommand, String responseCommand, Object requestData)
	{
		this.Part = part;
		this.PartElement = partElement;
		this.RequestCommand = requestCommand;
		this.ResponseCommand = responseCommand;
		
		this.RequestData = requestData;
	}
}

